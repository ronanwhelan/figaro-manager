<?php
/**
 * Created by PhpStorm.
 * User: ronanwhelan
 * Date: 1/6/16
 * Time: 8:42 AM
 */

namespace App\Figaro\PunchList;


use App\User;

class Emails {

    public function sendWeeklyUpdateMail(){

        $user = User::find(1);
        $email = 'ronanj.whelan@gmail.com';
        $stats = ['raised-seven-days' => 33,'closed-seven-days' => 12,'total' => 123];
        $count = 0;

        for($i = 0;$i < 2;$i++){
            \Mail::queue('figaro.email.punch-list-data', ['user' => $user, 'stats' => $stats], function ($m) use ($user, $email) {
                $m->from('info@projectfigaro.com', 'Project Figaro');
                $m->to($user->email, $user->name)->subject('Figaro Update');
            });

            $count ++;
        }
        return $count;

    }

}