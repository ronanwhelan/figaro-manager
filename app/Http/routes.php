<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::auth();

Route::get('/home', 'HomeController@index');



/*-------------------------------------------------
|                   EMAIL
 --------------------------------------------------*/

Route::get('/email', 'HomeController@email');
Route::get('/email/punch-list-data', 'HomeController@viewPunchListData');
Route::post('/email/send', 'HomeController@sendEmail');

/*-------------------------------------------------
|                   DATABASE
 --------------------------------------------------*/
Route::get('/database', 'HomeController@database');
Route::get('/database/backup-now', 'HomeController@backUpDatabaseNow');
Route::post('/database/restore', 'HomeController@restoreDatabaseNow');

/*-------------------------------------------------
|                   CSV EXPORT
 --------------------------------------------------*/
Route::get('/export/csv/tasks', 'ExportDBTablesController@exportTasksToCsv');
