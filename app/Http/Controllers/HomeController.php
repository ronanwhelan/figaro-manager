<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Storage;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }


    /*-------------------------------------------------
    |                  DATABASE
     --------------------------------------------------*/

    public function database()
    {

        $now = Carbon::now();
        $year = $now->year;
        $month = $now->month;

        $months = array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December',);

        $directory = '/db-backups/' . $year . '/' . $months[$month - 1];
        $fileList = Storage::disk('s3')->allFiles($directory);
        $directories = Storage::disk('s3')->allDirectories($directory);

        $bucketName = env('AWS_BUCKET');

        //https://s3-ap-southeast-1.amazonaws.com/abbvie-asia-bucket/uploads/2016-04-26-photo.JPG
        $region = env('AWS_REGION');
        $awsLink = "https://s3-ap-southeast-1.amazonaws.com/" . $bucketName .'/';

        return view('figaro.database.index', [
            'fileList' => $fileList,
            'awsLink' => $awsLink,
            'month' => $months[$month - 1],
        ]);

        //$schedule->exec('node /home/forge/script.js')->daily();
        // exec('node /home/forge/script.js');
    }

    public function backUpDatabaseNow()
    {

        $now = Carbon::now();
        //$month =
        $year = $now->year;
        $month = $now->month;
        $date = $now->toW3cString();

        $months = array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December',);

        $destinationPath = $year . '/' . $months[$month - 1] . '/' . $date;

        $message = \Artisan::call('db:backup',
            [
                '--database' => 'mysql',
                '--destination' => 's3',
                '--destinationPath' => $destinationPath,
                '--compression' => 'gzip',
            ]);

        $message = 'Database Backed Up to Cloud Storage';

        return \Redirect::back()->with('db-backup-message', $message);
    }



    //Restore DB
    public function restoreDatabaseNow(Request $request)
    {

        $this->validate($request, [
            'db_name' => 'required',//
            'db_connection' => 'required',//
            'file_name' => 'required',
        ]);

        $fileName = $request->file_name;

        try {
            $message = 'Database Restored';
            //db:restore --source=s3 --sourcePath=/2016/June/2016-06-01-11:47:05.gz --database=mysql --compression=gzip
            $artisan_message = \Artisan::call('db:restore',
                [
                    '--source' => 's3',
                    '--sourcePath' => $fileName,
                    '--database' => 'mysql2',
                    '--compression' => 'gzip',
                ]);

            return \Redirect::back()->with('restore-message', $message . ' Artisan message'. $artisan_message);

        } catch (\Exception $e) {
            $message = 'Error : ' . $e->getMessage();
            return \Redirect::back()->with('restore-message', $message);

        }

    }
    /*
    |--------------------------------------------------------------------------
    | Create Database
    |--------------------------------------------------------------------------
    |
    | Build a Database
    |
    */
    public function createDB($name){
        $query = sprintf("CREATE SCHEMA `%s` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ;", $name);
        $message = \DB::getPdo()->exec($query);
        return $message;
    }


    /*-------------------------------------------------
    |                   EMAIL
     --------------------------------------------------*/

    public function email()
    {
        return view('figaro.email.index');
    }

    public function viewPunchListData()
    {
        $user = \Auth::user();
        $stats = ['raised-seven-days' => 33, 'closed-seven-days' => 12, 'total' => 123];

        $data = ['user' => $user, 'stats' => $stats];

        return view('figaro.email.punch-list-data', $data);
    }


    public function sendEmail()
    {

        $user = \Auth::getUser();
        $email = 'ronanj.whelan@gmail.com';

        $stats = ['raised-seven-days' => 33, 'closed-seven-days' => 12, 'total' => 123];

        \Mail::queue('figaro.email.punch-list-data', ['user' => $user, 'stats' => $stats], function ($m) use ($user, $email) {
            $m->from('info@projectfigaro.com', 'Figaro Project Update');
            $m->to($email, $user->name)->subject('Update on recent events in your project');
        });

        return \Redirect::back()->with('email-message', 'Email to  ' . $user->name . ' sent');

    }


}
