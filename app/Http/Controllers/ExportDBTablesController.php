<?php

namespace App\Http\Controllers;


use App\Task;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\User;
use Illuminate\Http\Request;
use League\Csv\Writer;

use App\Http\Requests;

class ExportDBTablesController extends Controller
{
    public function exportTasksToCsv(){

        $tasks = Task::select(\DB::raw('
                    number as task_number,
                    description as task_description,
                    status,
                    complete,
                    target_val,
                    earned_val,
                    (select name from stages where id  = stage_id) as stage,
                    (select name from areas where id  = area_id) as area,
                    (select tag from systems where id  = system_id) as system,
                    (select name from groups where id  = group_id) as task_group,
                    (select name from task_types where id  = task_type_id) as category,
                    (select name from task_sub_types where id  = task_sub_type_id) as sub_category,
                    (select name from roles where id  = group_owner_id) as owner,
                    schedule_number

                    '))
            //->where('status', '<>', 1)
            // ->groupBy('status')
            ->get();

        $csv = \League\Csv\Writer::createFromFileObject(new \SplTempFileObject());

       // $csv->insertOne(\Schema::getColumnListing('users'));

        $csv->insertOne([
            'number', 'description',
            'status', 'complete', 'target_val', 'earned_val',
            'stage',
            'area','system',
            'task_group', 'category','sub_category',
            'owner','schedule Link'
        ]);

        foreach ($tasks as $task) {
            $csv->insertOne($task->toArray());
        }

        $fileName = 'tasks-'.Carbon::today()->toDateString().'.csv';

        $csv->output($fileName);


    }
}
