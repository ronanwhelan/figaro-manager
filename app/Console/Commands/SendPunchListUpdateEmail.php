<?php

namespace App\Console\Commands;

use App\Figaro\PunchList\Emails;
use App\User;
use Illuminate\Console\Command;


class SendPunchListUpdateEmail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'figaro:sendPunchListEmail';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sends a report email to all punch list users';


    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();


    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $weeklyMail = new Emails();
        $message = $weeklyMail->sendWeeklyUpdateMail();
        $this->comment(PHP_EOL . 'Email sent to ' . $message . ' Users ' . PHP_EOL);

    }
}
