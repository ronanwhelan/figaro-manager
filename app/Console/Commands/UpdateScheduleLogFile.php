<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;

class UpdateScheduleLogFile extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'figaro:updateScheduleLog';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Updates the Schedule log - used for testing';

    protected $date;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(Carbon $date)
    {
        parent::__construct();

        $this->date = $date->now();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        \Storage::disk('local')->append('JobsLog.txt', ' figaro:updateScheduleLog Called ' . $this->date);
    }
}
