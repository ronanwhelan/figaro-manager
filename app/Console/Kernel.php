<?php

namespace App\Console;

use Carbon\Carbon;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\Inspire::class,
        Commands\SendPunchListUpdateEmail::class,
        Commands\UpdateScheduleLogFile::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {

        //===== BACK UP DATABASE ======
        $now = Carbon::now();
        $year = $now->year;
        $month = $now->month;
        $date = $now->toW3cString();
        $months = array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December',);
        $destinationPath = $year . '/' . $months[$month - 1] . '/' . $date;
        //$schedule->command('db:backup --database=mysql --destination=s3 --destinationPath=`date +\%Y/%d-%m-%Y-%H:%M:%S` --compression=gzip')->twiceDaily(07,19);
        $schedule->command('db:backup --database=mysql --destination=s3 --destinationPath='.$destinationPath.' --compression=gzip')->twiceDaily(07,19);


        //===== TESTING ======
        //$schedule->command('figaro:updateScheduleLog')->everyThirtyMinutes();


        //$schedule->command('inspire')->hourly();
    }
}
