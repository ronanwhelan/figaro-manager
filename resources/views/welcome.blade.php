@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Welcome</div>

                <div class="panel-body">
                    <img src="/img/branding/figaro-logo.png" alt="..." class="img-rounded">
                    <h1 class="text-muted">Manager</h1>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
