<html>
<head></head>
<body style="">

<p>Hello {{$user->name}},</p>
<p>Here is your weekly update on the Punch List Items raised on the Abbvie Genisis Project </p>
<p>The list includes all items raised, are responsible for or have been assigned to your company. </p>
<ul class="list-group">
    <li class="list-group-item">{{$stats['raised-seven-days']}} New Items added in the last 7 Days </li>
    <li class="list-group-item">{{$stats['closed-seven-days']}} items were closed in the Last 7 days</li>
    <li class="list-group-item">{{$stats['total']}} Items in total </li>
</ul>
<p>please visit abbvie.projectfigaro.com/items/dashboard for more details </p>
<h4> The Figaro Team</h4>
</body>
</html>

