@extends('figaro.app.layouts.main')

@section('content')
    <div class="container">
        <div class="panel panel-default">
            <div class="panel-heading"><h3 class="panel-title">Email</h3></div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-12">
                        <div>
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Home</a></li>
                                <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Profile</a></li>
                                <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Messages</a></li>
                                <li role="presentation"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">Settings</a></li>
                            </ul>
                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="home">Home</div>
                                <div role="tabpanel" class="tab-pane" id="profile">Profile</div>
                                <div role="tabpanel" class="tab-pane" id="messages">...</div>
                                <div role="tabpanel" class="tab-pane" id="settings">...</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>




        <br>
        <br>
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-primary">
                    <div class="panel-heading"><h3 class="panel-title">Email</h3></div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <form action="/email/send" method="POST">
                                    {{ csrf_field() }}
                                    <div class="form-group">
                                        <label for="">To</label>
                                        <input type="email" name="email-to" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label for="">Message</label>
                                        <textarea name="message" class="form-control" rows="3"></textarea>

                                        <p class="help-block">Example block-level help text here.</p>
                                    </div>

                                    <button type="submit" class="btn btn-primary">Send</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>


    </div>

@endsection