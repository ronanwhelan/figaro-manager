@extends('figaro.app.layouts.main')

@section('content')
    <div class="container">


        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-info">
                    <div class="panel-heading">Database backup policy</div>
                    <div class="panel-body">
                        <h2>{{Config::get('figaro.company-name', 'something is wrong')}}</h2>
                        <ul>
                            <li>A full backup of the database is done twice daily</li>

                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-info">
                    <div class="panel-heading">Database Configuration</div>
                    <div class="panel-body">
                        <dl>
                            <dt>Schema</dt>
                            <dd>{{Config::get('database.connections.mysql.database')}}</dd>
                            <dt>Connection</dt>
                            <dd>{{Config::get('database.default')}}</dd>
                        </dl>
                    </div>
                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">Backup Database</div>

                    <div class="panel-body">
                        <dl>
                            <dt>Driver</dt>
                            <dd>{{Config::get('backup-manager.s3.type', 'something is wrong')}}</dd>
                            <dt>Region</dt>
                            <dd>{{Config::get('backup-manager.s3.region', 'something is wrong')}}</dd>
                            <dt>Bucket</dt>
                            <dd>{{Config::get('backup-manager.s3.bucket', 'something is wrong')}}</dd>
                            <dt>Folder Root</dt>
                            <dd>{{Config::get('backup-manager.s3.root', 'something is wrong')}}</dd>

                        </dl>

                        <a class="btn btn-success" href="/database/backup-now" role="button">Backup Database</a>

                        <p class="help-block"> Tables will be locked while backup in progress.</p>

                        @if (session('db-backup-message'))
                            <div class="alert alert-warning alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <strong>Info!</strong>{{ session('db-backup-message') }}
                            </div>
                        @endif


                    </div>


                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">Restore Database</div>

                    <div class="panel-body">
                        <form action="/database/restore" method="POST">
                            {!! csrf_field() !!}
                            <div class="form-group">
                                <label for="">DB Connection</label>
                                <p class="h4 text-primary">MYSQL</p>
                                <input type="hidden" class="form-control" id=""  name="db_connection" value="mysql">
                            </div>
                            <div class="form-group">
                                <label for="">New DB Name</label>
                                <p class="h4 text-primary">{{Config::get('figaro.company-name', 'abbvie')}}-{{\Carbon\Carbon::now()->toDateString()}}</p>
                                <input type="hidden" class="form-control" id="" name="db_name"  value="{{Config::get('figaro.company-name', 'staging')}}-{{\Carbon\Carbon::now()->toDateString()}}">
                            </div>
                            <div class="form-group">
                                <label for="">Backup Filename</label>
                                <input type="text" class="form-control" id="" name="file_name" placeholder="Note: 'db-backups' prefix is hard coded. No need to add e.g /2016/July/2016-07-20T12:19:56+08:00.gz">
                            </div>

                            <div class="checkbox hide">
                                <label>
                                    <input type="checkbox"> Make Backup first
                                </label>
                            </div>
                            <button type="submit" class="btn btn-warning">Restore Database</button>


                            @if (count($errors) > 0)
                                <br>
                                <br>
                                <div class="alert alert-danger">
                                    <p><strong>Validation Errors!</strong></p>
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif


                        </form>

                        <p class="help-block">This action must be done with caution. Users will not be able to use the app while
                            the database is being restored</p>


                        @if (session('restore-message'))
                            <div class="alert alert-warning alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <strong>Info! </strong>{{ session('restore-message') }}
                            </div>
                        @endif

                        <hr>
                        <h3>List of back up files for {{$month}} </h3>
                        <!-- Table -->
                        <table class="table">
                            <thead>
                            <tr>
                                <th>File Name</th>
                                <th>Download Link</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($fileList as $file)
                                <tr>
                                    <th scope="row">{{$file}}</th>
                                    <td><a href="{{$awsLink}}{{$file}}">{{$file}}</a></td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>


        <div class="row hide">
            <div class="col-md-12">
                <div class="panel panel-info">
                    <div class="panel-heading">Scheduling Backups</div>

                    <div class="panel-body">

                        <h4>Database manager allows one to configure the database backup scheduling, Backup and Restore a database</h4>
                        <br>
                        <button type="button" class="btn btn-primary">Backup Database</button>
                        <button type="button" class="btn btn-warning">Restore Database</button>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection